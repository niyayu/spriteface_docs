Web API文档
==================

使用SpriteFace的Web API，即可以获取来自SpriteFace摄像头的客流统计数据和VIP识别结果。

获取API Token
-------------

所有的Web API调用（除了该API）都需要附加Token参数进行权限验证。

.. http:post:: /auth

    根据用户名和密码，该API能够获取一个20分钟内有效的Token。

    :<json string user: 用户名
    :<json string password: 密码

    :status 200: 验证成功
    :status 400: 验证失败，用户名或密码错误

    :>json string token: API token, 例如eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "token": eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=
        }

VIP录入
-------

VIP录入分为两步：首先是上传人脸视频并获得一个查询token（不同于上面的API Token），然后
根据token不断轮询VIP录入的结果，直到返回Success。

.. http:post:: /videos

    上传VIP的录入视频。

    :>header Authorization: 示例: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    :form name: VIP的姓名
    :form mobile: VIP的手机号
    :form file: 视频文件

    :status 200: 上传成功
    :status 400: 上传失败

    :>json int userID: 生成的VIP ID
    :>json string token: VIP录入的查询Token

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "userID": 1,
            "token": "lidnt3c8341cja03z"
        }

.. http:get:: /videoStatus

    根据Token，查询VIP录入的结果。

    :>header Authorization: 示例: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    :query string token: /videos 返回的查询Token

    :status 200: 查询成功
    :status 400: Token不存在

    :>json string token: 相同的查询Token
    :>json string status: "pending":还未开始处理; "processing":正在处理视频; "success":处理完成
    :>json string result: 处理结果的文字描述（"录入成功"，"未采集到足够正脸"，"未采集到足够低头的人脸"，"未采集到足够左转的人脸"，"未采集到足够右转的人脸"）

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "token": "lidnt3c8341cja03z",
            "status": "success",
            "result": "未采集到足够低头的人脸"
        }


历史到店记录查询
---------------

.. http:get:: /identities

    根据筛选条件，查询相应的到店记录

    :>header Authorization: 示例: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    :query string vipID: （可选）只查询某个VIP的到店历史
    :query string cameraID: （可选）只查询某个摄像头下的到店历史
    :query string ageLvel: （可选）只查询某个年龄段的到店历史（只支持三种参数：'0-20', '20-40', '40-100'）
    :query string gender: （可选）只查询某个性别的到店历史（只支持两种参数：'male', 'female'）
    :query string date: （可选）只查询某个日期的到店历史（格式：YYYY-MM-DD）

    :status 200: 查询成功
    :status 400: 查询失败

    :>json int total_count: 返回的历史记录数量
    :>json array items: 返回的历史记录明细
    :>jsonarr int items.personID: 到店顾客的ID
    :>jsonarr string items.vipID: 如果到店顾客是VIP，则返回其VIP ID，否则返回NULL
    :>jsonarr int items.age: 到店顾客的年龄
    :>jsonarr string items.gender: 到店顾客的性别
    :>jsonarr string items.date: 到店顾客的日期
    :>jsonarr array items.trackerLogs: 顾客被摄像头采集的轨迹记录
    :>jsonarr int items.trackerLogs.trackerID: 轨迹点ID
    :>jsonarr string items.trackerLogs.cameraID: 轨迹点对应的摄像头ID
    :>jsonarr string items.trackerLogs.startAt: 顾客在该轨迹点的出现时间
    :>jsonarr string items.trackerLogs.endAt: 顾客在该轨迹点的离开时间
    :>jsonarr string items.trackerLogs.faceImageUrl: 顾客在该轨迹点被采集的头像地址

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "total_count": 2,
            "items": [{
                "personID": 123,
                "vipID": "NULL",
                "age": 30,
                "gender": "male",
                "date": "2018-11-11",
                "trackerLogs": [{
                    "trackerID": 123,
                    "cameraID": "abc3kda",
                    "startAt": "2018-11-11T10:00:00Z",
                    "endAt": "2018-11-11T10:01:12Z",
                    "faceImageUrl": "http://example.com/face/1.jgp",
                }, {
                    "trackerID": 124,
                    "cameraID": "djck382",
                    "startAt": "2018-11-11T10:02:03Z",
                    "endAt": "2018-11-11T10:04:01Z",
                    "faceImageUrl": "http://example.com/face/2.jgp",
                }]
            }, {
                "personID": 124,
                "vipID": "102",
                "age": 29,
                "gender": "female",
                "date": "2018-11-11",
                "trackerLogs": [{
                    "trackerID": 125,
                    "cameraID": "abc3kda",
                    "startAt": "2018-11-11T11:00:00Z",
                    "endAt": "2018-11-11T11:01:12Z",
                    "faceImageUrl": "http://example.com/face/3.jgp",
                }]
            }]
        }


历史到店统计数据
--------------

.. http:get:: /identityStats/dailyCount

    按天查询某个摄像头下的不重复到店人数

    :>header Authorization: 示例: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    :query string startDate: 开始日期（格式：YYYY-MM-DD）
    :query string endDate: 截止日期（格式：YYYY-MM-DD）
    :query string cameraID: （可选）只查询的某个摄像头的数据

    :status 200: 查询成功
    :status 400: 查询失败

    :>json array all: 所有人独立到店数据
    :>json array vip: VIP独立到店数据
    :>json array notVip: 非VIP独立到店数据

    :>jsonarr string all.date: 所有人独立到店数据对应的日期
    :>jsonarr int all.count: 所有人独立到店数量
    :>jsonarr float all.dayOnDay: 所有人独立到店对比前一天的比例
    :>jsonarr float all.weekOnWeek: 所有人独立到店对比上周同一天的比例

    :>jsonarr string vip.date: VIP独立到店数据对应的日期
    :>jsonarr int vip.count: VIP独立到店数量
    :>jsonarr float vip.dayOnDay: VIP独立到店对比前一天的比例
    :>jsonarr float vip.weekOnWeek: VIP独立到店对比上周同一天的比例

    :>jsonarr string notVip.date: 非VIP独立到店数据对应的日期
    :>jsonarr int notVip.count: 非VIP独立到店数量
    :>jsonarr float notVip.dayOnDay: 非VIP独立到店对比前一天的比例
    :>jsonarr float notVip.weekOnWeek: 非VIP独立到店对比上周同一天的比例

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "all": [{
                "date": "2018-11-01",
                "count": 1000,
                "dayOnDay": 1.11,
                "weekOnWeek": 0.55
            }, {
                "date": "2018-11-02",
                "count": 1000,
                "dayOnDay": 1.11,
                "weekOnWeek": 0.55
            }],
            "vip": [{
                "date": "2018-11-01",
                "count": 500,
                "dayOnDay": 1.11,
                "weekOnWeek": 0.55
            }, {
                "date": "2018-11-02",
                "count": 500,
                "dayOnDay": 1.11,
                "weekOnWeek": 0.55
            }],
            "notVip": [{
                "date": "2018-11-01",
                "count": 500,
                "dayOnDay": 1.11,
                "weekOnWeek": 0.55
            }, {
                "date": "2018-11-02",
                "count": 500,
                "dayOnDay": 1.11,
                "weekOnWeek": 0.55
            }]
        }

.. http:get:: /identityStats/monthCount

    按月查询某个摄像头下的不重复到店人数

    :>header Authorization: 示例: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    :query string startMonth: 开始日期（格式：YYYY-MM）
    :query string endMonth: 截止日期（格式：YYYY-MM）
    :query string cameraID: （可选）只查询的某个摄像头的数据

    :status 200: 查询成功
    :status 400: 查询失败

    :>json array all: 所有人独立到店数据
    :>json array vip: VIP独立到店数据
    :>json array notVip: 非VIP独立到店数据

    :>jsonarr string all.month: 所有人独立到店数据对应的月份
    :>jsonarr int all.count: 所有人独立到店数量
    :>jsonarr float all.monthOnMonth: 所有人独立到店对比上个月的比例
    :>jsonarr float all.yearOnYear: 所有人独立到店对比去年同一月的比例

    :>jsonarr string vip.month: VIP独立到店数据对应的月份
    :>jsonarr int vip.count: VIP独立到店数量
    :>jsonarr float vip.monthOnMonth: VIP独立到店对比上个月的比例
    :>jsonarr float vip.yearOnYear: VIP独立到店对比去年同一月的比例

    :>jsonarr string notVip.month: 非VIP独立到店数据对应的月份
    :>jsonarr int notVip.count: 非VIP独立到店数量
    :>jsonarr float notVip.monthOnMonth: 非VIP独立到店对比上个月的比例
    :>jsonarr float notVip.yearOnYear: 非VIP独立到店对比去年同一月的比例

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "all": [{
                "month": "2018-11",
                "count": 10000,
                "monthOnMonth": 1.11,
                "yearOnYear": 0.55
            }, {
                "month": "2018-12",
                "count": 10000,
                "monthOnMonth": 1.11,
                "yearOnYear": 0.55
            }],
            "vip": [{
                "month": "2018-11",
                "count": 5000,
                "monthOnMonth": 1.11,
                "yearOnYear": 0.55
            }, {
                "month": "2018-12",
                "count": 5000,
                "monthOnMonth": 1.11,
                "yearOnYear": 0.55
            }],
            "notVip": [{
                "month": "2018-11",
                "count": 5000,
                "monthOnMonth": 1.11,
                "yearOnYear": 0.55
            }, {
                "month": "2018-12",
                "count": 5000,
                "monthOnMonth": 1.11,
                "yearOnYear": 0.55
            }]
        }

.. http:get:: /identityStats/dailyDuration

    查询到店顾客的停留时间（单位：秒）

    :>header Authorization: 示例: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9=

    :query string startDate: 开始日期（格式：YYYY-MM-DD）
    :query string endDate: 截止日期（格式：YYYY-MM-DD）
    :query string cameraID: （可选）只查询的某个摄像头的数据

    :status 200: 查询成功
    :status 400: 查询失败

    :>json array all: 所有人独立到店数据
    :>json array vip: VIP独立到店数据
    :>json array notVip: 非VIP独立到店数据

    :>jsonarr string all.date: 所有人独立到店数据对应的日期
    :>jsonarr int all.high: 所有人的最长停留时间
    :>jsonarr int all.low: 所有人的最短停留时间
    :>jsonarr int all.median: 所有人停留时间的中位数
    :>jsonarr int all.lowerQuarter: 所有人停留时间的下四分位数
    :>jsonarr int all.upperQuarter: 所有人停留时间的上四分位数

    :>jsonarr string vip.date: VIP独立到店数据对应的日期
    :>jsonarr int vip.high: VIP的最长停留时间
    :>jsonarr int vip.low: VIP的最短停留时间
    :>jsonarr int vip.median: VIP停留时间的中位数
    :>jsonarr int vip.lowerQuarter: VIP停留时间的下四分位数
    :>jsonarr int vip.upperQuarter: VIP停留时间的上四分位数

    :>jsonarr string notVip.date: 非VIP独立到店数据对应的日期
    :>jsonarr int notVip.high: 非VIP的最长停留时间
    :>jsonarr int notVip.low: 非VIP的最短停留时间
    :>jsonarr int notVip.median: 非VIP停留时间的中位数
    :>jsonarr int notVip.lowerQuarter: 非VIP停留时间的下四分位数
    :>jsonarr int notVip.upperQuarter: 非VIP停留时间的上四分位数

    **返回结果示例**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "all": [{
              "date": "2018-11-01",
              "high": 100,
              "low": 20,
              "lowerQuarter": 40,
              "median": 50,
              "upperQuarter": 80
            },{
              "date": "2018-11-02",
              "high": 100,
              "low": 20,
              "lowerQuarter": 40,
              "median": 50,
              "upperQuarter": 80
            }],
            "vip": [{
              "date": "2018-11-01",
              "high": 100,
              "low": 20,
              "lowerQuarter": 40,
              "median": 50,
              "upperQuarter": 80
            },{
              "date": "2018-11-02",
              "high": 100,
              "low": 20,
              "lowerQuarter": 40,
              "median": 50,
              "upperQuarter": 80
            }],
            "notVip": [{
              "date": "2018-11-01",
              "high": 100,
              "low": 20,
              "lowerQuarter": 40,
              "median": 50,
              "upperQuarter": 80
            },{
              "date": "2018-11-02",
              "high": 100,
              "low": 20,
              "lowerQuarter": 40,
              "median": 50,
              "upperQuarter": 80
            }]
        }
