产品性能
========

.. list-table::
    :widths: 100 100

    * - **硬件**
      -
    * - CPU
      - RK3399
    * - 操作系统
      - Ubuntu 16.04
    * - **资源消耗**
      -
    * - CPU占用
      - 200%到300%
    * - RAM占用
      - 200MB
    * - 存储占用
      - 50M
    * - **VIP识别性能**
      -
    * - 配置
      - 底库人数：10000人
    * - 实时人数
      - 最大50人
    * - 距离：1米
      - Precision=99%, Recall=99%
    * - 距离：2米
      - Precision=97%, Recall=95%
    * - 距离：3米
      - Precision=95%, Recall=90%
    * - 距离：4米
      - Precision=93%, Recall=85%
